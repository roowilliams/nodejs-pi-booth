pkg = require('../package.json');

module.exports = function (app) {

	app.get('/', function(req, res) {
		// passing over the title dynamically. This might be bad practice
		// due to coupling of content and logic?
		res.render('client', {
			title: pkg.name, 
			bodyclass: "client" 
		});
	});

	app.get('/local', function(req, res) {
		// passing over the title dynamically. This might be bad practice
		// due to coupling of content and logic?
		res.render('local', {
			title: pkg.name, 
			bodyclass: "local" 
		});
	});


	// 404 catch-all handler (middleware)
	app.use(function(req, res, next){
		res.status(404);
		res.render('404');
	});


	// 500 error handler (middleware)
	app.use(function(err, req, res, next) {
		console.error(err.stack);
		res.status(500);
		res.render('500');
	});


};