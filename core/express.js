var express = require('express'),
	pkg = require('../package.json');


module.exports = function (app, config) {

	var handlebars = require('express-handlebars').create({ defaultLayout:'main' }),
	bodyParser = require('body-parser');

	app.engine('handlebars', handlebars.engine);
	app.set('view engine', 'handlebars');


	app.use(express.static(config.root + '/public'));

	app.use(bodyParser.json());       // to support JSON-encoded bodies
	app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
		extended: true
	}));

};