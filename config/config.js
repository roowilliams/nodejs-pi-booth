var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'), //sets root path
    config,
    privconfig = require('./privconfig');

config = {


	
	app: {
		name: 'Pi-Booth',
		port:   8080,
		url:    ''
	},
	
	root: rootPath,
	resourceURI: privconfig.resourceURI,

	updateFrequency: 7200000, // every 2 hours
	photoPath: './public/images/portraits/',
	webPhotoPath: 'images/portraits/',
	flashPin: 7,
	previewTimeout: 5000,
	previewDims: '282,20,460,558',
	//previewDims: '\'282,20,460,558\'',
	imgw : 768,
	imgh : 1024,
	awb : 'auto',
	brightness : 60,
	contrast : 40,
	saturation : -32,
	sharpness : 10,

	email_domain : '@tmwunlimited.com',
	sessionTimeout : 60000 * 1, // 1 mins

	intranetUser : privconfig.intranetUser,
	intranetPass : privconfig.intranetPass,
	sendURL : privconfig.sendURL,

	appSteps : 4,
	localDisplay: true,

	screenSaverInterval : 60000 * 0.1,
	screenSaverDuration : 4000,

	dev: {
		piUser: privconfig.piUser,
		piPassword: privconfig.piPassword
	}



};


// Export config
module.exports = config;
