var PiBooth = PiBooth || {};

PiBooth = {

	session : {},

	messageStrings : {
		loginFail : 'Sorry %s doesn\'t exist',
		loginSuccess : 'Hello %s, please click the button below to take a photo.',
		inUse : 'Sorry the photobooth is currently in use. Stay on this page and I\'ll let you know when, it\'s free again.',
		ready : 'The photobooth is no longer in use, please log in below.',
		timeout : 'You have been logged out due to inactivity.',
		begin : 'Please enter your username below',
		end : 'Thanks! I\'ve emailed your photo to %s.',
		sending : 'Sending...',
		sentOK : 'Photo'
	},

	init : function () {
		console.log("script.js :: init()");

		this.SocketController.init();

		this.EventListeners.onPageStart();

	},



	EventListeners : {
		onPageStart : function () {

			console.log('script.js :: event :: onPageStart');
			_this.UI.init();


		}

	},

	SocketController : {

		socket : null,

		init : function () {

			console.log('script.js :: making connection');
			var connectionURL = window.location.hostname;
			this.socket = io.connect(connectionURL);

			this.setUpListeners();


		},

		emitMsg : function (msg, data) {

			this.socket.emit(msg, data);

		},



		setUpListeners : function() {

			_this.SocketController.socket.on('response', function(response) {

				_this.UI.loginResponse(response);


			});

			_this.SocketController.socket.on('photo', function(filepath) {

				_this.UI.showPhoto(filepath);

			});

			_this.SocketController.socket.on('timeout', function() {

				_this.UI.timeOut();
				console.log('timeout');

			});

			_this.SocketController.socket.on('ready', function() {

				_this.UI.getReady();

			});

			_this.SocketController.socket.on('sending', function() {

				_this.UI.sending();

			});

			_this.SocketController.socket.on('sentok', function() {

				_this.UI.sentOK();

			});


			_this.SocketController.socket.on('screensave', function(time) {

				_this.Local.screenSaver(time);

			});
		}
	},





	UI : {

		init : function() {

			this.EventListeners.buttonListener();


		},

		login : function(input) {

			console.log(input);

			if (input) {
				_this.SocketController.emitMsg('login', input);
			}

		},

		loginResponse : function(response) {

			console.log('loginResponse', response);

			switch(response.code) {

				case 0:
				_this.UI.sendMsg(this.createMsg(_this.messageStrings.loginFail,response.input), 'alert alert-danger');
				break;


				case 1:

				function capitalizeFirstLetter(string) {
					return string.charAt(0).toUpperCase() + string.slice(1);
				}
				var name = capitalizeFirstLetter(response.user.firstName);

				_this.UI.sendMsg(this.createMsg(_this.messageStrings.loginSuccess,name), 'alert alert-info');
				_this.session.user = response.user;
				_this.UI.controlState();
				break;


				case 2:
				_this.UI.sendMsg(_this.messageStrings.inUse, 'alert alert-warning');
				break;
			}

			
		},


		end : function () {
			_this.SocketController.emitMsg('end');
		},


		restart : function () {
			_this.UI.beginState();
		},


		beginState : function() {
			$('#end-dialog').hide();
			$('#image-preview-dialog').hide();
			$('#photo-controls-dialog').hide();
			$('#login-dialog').show();
			$('#messaging').text(_this.messageStrings.begin);
		},


		controlState : function () {

			$('#login-dialog').hide();
			$('#image-preview-dialog').hide();
			$('#photo-controls-dialog').show();

		},


		evaluateState : function () {

			$('#photo-controls-dialog').hide();
			$('#image-preview-dialog').show();

		},


		endState : function () {

			$('#image-preview-dialog').hide();
			$('#end-dialog').show();
			_this.UI.sendMsg(this.createMsg(_this.messageStrings.end, _this.session.user.email), 'alert alert-info');

		},


		timeOut : function () {

			_this.UI.sendMsg(_this.messageStrings.timeout, 'alert alert-warning');
			$('#login-dialog').hide();
			$('#photo-controls-dialog').hide();
			$('#image-preview-dialog').hide();
			$('#end-dialog').show();

		},


		getReady : function () {
			$('#messaging').text(_this.messageStrings.ready);
		},


		takePhoto : function(user) {
			_this.SocketController.emitMsg('takephoto', user);
			$('#messaging').removeClass();
			$('#messaging').text('');
		},


		showPhoto : function(filepath) {

			$('#photo-preview').attr("src",filepath);
			_this.UI.evaluateState();

		},

		sending : function() {
			console.log('Sending...');
		},

		sentOK : function() {
			_this.UI.endState();
		},

		sendMsg : function (msg, classes) {

			if (!classes) {
				$('#messaging').removeClass();
			}
			else {
				$('#messaging').addClass(classes);
			}

			$('#messaging').text(msg);

			
		},


		createMsg : function (str, variable) {
			var msg = str.replace('%s',variable);
			return msg;
		},


		EventListeners : {

			onPageStart : function () {

				this.buttonListener();

			},



			buttonListener : function () {

				$('#login-submit').on('click', function(event) {
					_this.UI.login($('#user-input').val());
					event.preventDefault();
				});


				$('#takephoto').on('click', function(event) {
					_this.UI.takePhoto(_this.session.user);
					event.preventDefault();
				});


				$('#tryagain').on('click', function(event) {
					_this.UI.controlState();
					event.preventDefault();
				});


				$('#restart').on('click', function(event) {
					_this.UI.restart();
					event.preventDefault();
				});


				$('#usephoto').on('click', function(event) {
					_this.UI.end();
					event.preventDefault();
				});

				$('input').keypress(function(event) {
					if (event.which == 13) {
						_this.UI.login($('#user-input').val());
						event.preventDefault();
					}
				});

			},

		}
	},

	Local : {

		init : function() {

			_this.SocketController.emitMsg('local');

			console.log('Local :: init');

			_this.SocketController.socket.on('step', function(step) {

				_this.Local.showStep(step);

			});
		},

		showStep : function(step) {

			console.log('Local :: showStep ::' + step);

			for (var i = 1; i < 4; i++) {

				var element = '.' + 'step' + i;

				if (i === step) {
					$(element).show(); 
				}
				else {
					$(element).hide();
				}
			}
		},

		screenSaver : function (time) {
			var overlay = $('.overlay');


			overlay.addClass('screensave');


			timer = setTimeout(function() {
				overlay.removeClass('screensave');
			}, time);
		},
	}
};

var _this = PiBooth;



$(document).ready(function() {
	_this.init();
});