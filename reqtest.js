// var request = require('request');

// 	request
// 	.get ()
// 	config.intranetUser + ":" + config.intranetPass + " -F \"photo=@/home/pi/node-pi-booth/" +
// 			imagePath + ";type=application/octet-stream;\" -F \"guid=" +
// 			guid + config.sendURL

var needle = require('needle');
var config = require('./config/config');

var data = {
	photo : {

		file: 'image.jpg',
		content_type: 'application/octet-stream'

	},
	
	guid: 18090
};

needle.post(config.sendURL, data, {
	username: config.intranetUser,
	password: config.intranetPass,
	multipart: true,
	auth: 'auto',
	follow: 3,
	'rejectUnauthorized': false
}, function(err, resp, body) {
	console.log('err: ' + err);
	console.log('resp: ' + resp.statusCode);
	//console.log('body: ' + body);
});