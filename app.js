var express = require('express'),
	app = express(),
	server = require('http').createServer(app),
	config = require('./config/config'),
	EventEmitter = require('events').EventEmitter,
	port = process.env.PORT || config.app.port;

server.listen(port, function () {
	console.log('Server listening at port %d', port);
});


// express settings
require('./core/express')(app, config);

// Bootstrap routes
require('./core/routes')(app);

app.emitter = new EventEmitter();

app.listen(app.get('port'), function() {
	console.log( 'Express started on http://localhost:' +
		app.get('port') + '; press Ctrl-C to terminate.' );
});

var socketController = require('./controllers/SocketController');
socketController.setup(app, server, config);

var MsgController = require('./controllers/MsgController');
MsgController.init(app, server, socketController);



exports = module.exports = app;
