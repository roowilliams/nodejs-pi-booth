var io = require('socket.io'),
	MsgController = require('./MsgController'),
	pkg = require('../package.json');


var SocketController = {

	socketServer : null,

	setup : function (app, server, config) {

		_this.socketServer = io.listen(server);

		_this.socketServer.sockets.on('connection', function(socket) {

			MsgController.onNewSocketConnection(socket);

			socket.on('disconnect', function() {
				MsgController.onSocketClose(socket);
			});

			socket.on('error', function(error) {
				console.log('Socket.io :: ' + error);
			});

		});

		_this.socketServer.sockets.on('close', function(socket) {
			console.log('ui : socketServer has closed');
		});

	},

	emitMsg : function (msg, data) {

		_this.socketServer.sockets.emit(msg, data);

	},

	emitMsgToClient : function (socketId, msg, data) {

		_this.socketServer.sockets.connected[socketId].emit(msg, data);

	},
	
	broadcast : function (msg, data) {
		_this.socketServer.sockets.broadcast.emit(msg, data);
	}
};

var _this = SocketController;

module.exports = SocketController;
