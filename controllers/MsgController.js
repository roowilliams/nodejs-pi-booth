
// This is the main application controller.



var pkg = require('../package.json'),
config = require('../config/config'),
UsersController = require('./UsersController'),
fs = require('fs'),
CameraController = require('./CameraController'),
SendController = require('./SendController')
childProcess = require('child_process'); 


SocketController = null;

var MsgController = {

	localClientSocket : null,

	appStep : 1,

	session : {},

	clients : [],

	screenSaverRunning : null,

	init : function (app, server, socketController) {

		SocketController = socketController;
		UsersController.init(config.resourceURI, config.updateFrequency);

		// Open local browser window
		if (config.localDisplay) {
			childProcess.exec('/usr/bin/midori -e Fullscreen -a http://localhost/local');
		}
		_this.screenSaverInit(config.screenSaverInterval);

	},


	screenSaverInit : function(interval) {

		_this.screenSaverRunning = setInterval(function() {
			_this.emitScreenSaveEvent();
		}, interval);
	},


	screenSaverStop : function() {
		if (_this.screenSaverRunning) {
			clearInterval(_this.screenSaverRunning);
		}
	},


	emitScreenSaveEvent : function() {
		if (_this.localClientSocket) {
			SocketController.emitMsgToClient(_this.localClientSocket, 'screensave', config.screenSaverDuration);
		}
	},


	onNewSocketConnection : function(socket) {

		var clientIp = socket.request.connection.remoteAddress;

		console.log('New connection logged from', clientIp);

		_this.UI.setUpSocketEventListeners(socket);

		_this.appStep = 2;

		if (_this.localClientSocket) {
			SocketController.emitMsgToClient(_this.localClientSocket, 'step', _this.appStep);
		}
	},

	onSocketClose : function (socket) {

		if (socket.id == _this.session.socketId) {
			_this.clearSession();
		}

		if (socket.id == _this.localClientSocket) {
			_this.localClientSocket = null;
			return;
		}

		else {
			console.log('Clients');
			delete _this.clients.socketId;
		}

		console.log('Client disconnected.');
	},

	onNewImage : function(filename) {

		if (_this.session.filePath) {

			fs.unlinkSync(_this.session.filePath);
		}

		var fileDir = config.photoPath,
		filePath = fileDir + filename,
		newName = filename + '-' + Date.now() + '.jpg';
		webFilePath = config.webPhotoPath + newName;


		fs.renameSync(filePath, fileDir + newName);

		console.log(fileDir+newName);

		_this.session.filePath = fileDir+newName;

		SocketController.emitMsgToClient(_this.session.socketId, 'photo', webFilePath);
	},

	onNewLogin : function (input, socket) {

		var socketId = socket.id;

		input = input.toLowerCase().trim();

		var response = {};
		
		response = UsersController.getUser(input);

		if (Object.keys(_this.session).length === 0) {

			if (!response.input) {

				response.code = 1;

				CameraController.init(response.user);

				CameraController.on('newphoto', function(file) {
					_this.onNewImage(file);
				});

				_this.session.user = response.user;
				_this.session.socketId = socketId;

				_this.sessionTimer();

				_this.clients.shift();

				_this.appStep = 3;

				_this.screenSaverStop();

				if (_this.localClientSocket) {
					SocketController.emitMsgToClient(_this.localClientSocket, 'step', _this.appStep);
				}
			}

			else {
				response.code = 0;
			}
		}

		else {
			var clientIp = socket.request.connection.remoteAddress;

			_this.clients.push({ 
				'socketId': socketId, 
				'clientIp': clientIp
			});
			response.code = 2;
		}

		SocketController.emitMsgToClient(socketId, 'response', response);

		console.log('Input: %s', input);
		console.log('Response: %s', JSON.stringify(response));

	},

	sessionTimer : function(reset) {


		if (reset) { // if client still active

			clearTimeout(_this.session.timeout);

		}

		_this.session.timeout = setTimeout(function() {
			_this.clearSession('timeout');
		}, config.sessionTimeout);


	},

	clearSession : function (state) {

		if (Object.keys(_this.session).length > 0) {

			console.log(_this.session);

			clientStatusUpdate(state);

			console.log('Cleared session for user: ', _this.session.user.username);

			clearTimeout(_this.session.timeout);
			_this.screenSaverInit(config.screenSaverInterval);

			CameraController.stop();

			_this.session = {};
			_this.appStep = 1;
			
			if (_this.localClientSocket) {
				SocketController.emitMsgToClient(_this.localClientSocket, 'step', _this.appStep);
			}

			function clientStatusUpdate(state) {
				console.log('clientStatusUpdate :: ' + state + ' :: ' + _this.clients);

				if (state === 'timeout') {
					// Tell session user htey have timed out due to inactivity.
					SocketController.emitMsgToClient(_this.session.socketId, 'timeout');

					if (_this.clients.length > 0) {
					// Tell next client in queue that they can log in.
					SocketController.emitMsgToClient(_this.clients[0].socketId, 'ready');
				}

			}

		}

	}
	else {
		console.log('No logged in users.');
	}

},




UI : {

	setUpSocketEventListeners : function(socket) {

		socket.on('local', function() {

			_this.localClientSocket = socket.id;

		});

		socket.on('login', function(input) {

			_this.onNewLogin(input, socket);


		});

		socket.on('takephoto', function() {

			CameraController.getPhoto();
			_this.sessionTimer(true);

		});


		socket.on('restart', function() {

			_this.clearSession();

		});


		socket.on('end', function() {
			SendController.send(_this.session.filePath, _this.session.user.guid);

			SocketController.emitMsgToClient(_this.session.socketId, 'sending');

			SendController.on('response', function(code) {
				console.log('response: ' + code);
				if (code === 200) {

					SocketController.emitMsgToClient(_this.session.socketId, 'sentok');
					_this.clearSession();

				}

			});



		});


		socket.on('error', function(error) {

			console.error(error.stack);

		});

	}
}

};

var _this = MsgController;

module.exports = _this;