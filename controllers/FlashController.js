var pkg = require('../package.json'),
	gpio = require('rpi-gpio'),
	events = require('events'),
	config = require('../config/config'),
	MsgController = require('./MsgController');


var FlashController = {

	pin : config.flashPin,

	init : function () {

		gpio.setup(_this.pin, gpio.DIR_OUT);
		//_this.off();
		console.log('FlashController :: ready');
		
	},

	on : function() {
			
			gpio.write(_this.pin, 0, function(err) {
				if (err) throw err;
			});
		
		
	},


	off : function() {

			gpio.write(_this.pin, 1, function(err) {
				if (err) throw err;
			});
		
	}



};






var _this = FlashController;

module.exports = _this;
