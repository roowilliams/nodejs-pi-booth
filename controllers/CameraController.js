var pkg = require('../package.json'),
	raspicam = require('raspicam'),
	events = require('events'),
	config = require('../config/config'),
	FlashController = require('./FlashController'),
	RaspiCam = require('raspicam');


var CameraController = {

	camera : null,

	file : null,

	emitter : null,

	emit : function(event, args) {
		_this.emitter.emit(event, args);
	},

	on : function(event, fn) {
		_this.emitter.on(event, fn);
	},

	init : function (user) {


		FlashController.init();

		_this.emitter = new events.EventEmitter();

		//var filename = user.firstName + '_' + user.lastName + '-' + Date.now() + '.jpg';
		var filename = user.firstName + '_' + user.lastName;
		_this.camera = new RaspiCam({

			mode : 'photo',
			output : config.photoPath + filename,
			quality : 90,
			timeout : config.previewTimeout,
			preview : config.previewDims,
			width : config.imgw,
			height : config.imgh,
			awb : config.awb,
			brightness : config.brightness,
			contrast : config.contrast,
			saturation : config.saturation,
			sharpness : config.sharpness
		});

		console.log('CameraController :: init');
		_this.listeners();

	},

	getPhoto : function () {
		_this.camera.start();
	},

	newImageMade : function (filename) {
		
		_this.emitter.emit('newphoto', filename);
		
	},

	stop : function () {
		_this.camera.stop();
		_this.camera = null;
	},

	listeners : function () {

		//listen for the "start" event triggered when the start method has been successfully initiated
		_this.camera.on("start", function() {
			FlashController.on();
		});

		//listen for the "read" event triggered when each new photo/video is saved
		_this.camera.on("read", function(err, timestamp, filename) {
			_this.file = filename;
		});

		//listen for the "stop" event triggered when the stop method was called
		_this.camera.on("stop", function() {
		});

		//listen for the process to exit when the timeout has been reached
		_this.camera.on("exit", function() {
			FlashController.off();
			_this.newImageMade(_this.file);
		});
	}

};



var _this = CameraController;


module.exports = _this;
