var request = require('request'),
pkg = require('../package.json'),
//debug = require('request').debug = true,
fs = require("fs"),
config = require('../config/config');

UsersController = {

	users : {},

	init : function (resourceURI, updateFrequency) {

		console.log('UsersController :: init');

		_this.Scrape.run(resourceURI);

		scraper = setInterval(function() {
			_this.Scrape.run(resourceURI);
		}, updateFrequency);
	},

	getUser : function (input) {

		var response = null;

		if (_this.users[input]) {
			response = { 'user' : _this.users[input] };
			return response;
		}
		else {
			response = { 'input' : input };
			return response;
		}

	},


	Scrape : {

		run : function(resourceURI) {

			// Fetch userlist from intranet
			
			request({
				url : resourceURI,
				rejectUnauthorized : false

			}, function(error, response, data) {

				if (!error) {

					// Create users object from returned data
					
					var array = data.toString().split('\n');


					for (var i in array) {
						
						if (array[i]) {

							var line = array[i].split(',');

							var guid = line[0].trim().toLowerCase(),
							username = line[1].trim().toLowerCase(),
							fullName = line[2].trim().toLowerCase(),
							email = line[3].trim().toLowerCase(),

							firstName = fullName.split(' ')[0],
							lastName = fullName.split(' ')[1];


							if (parseInt(guid)) {

								_this.users[username] = {
									guid : guid,
									username : username,
									fullName : fullName,
									firstName : firstName,
									lastName : lastName,
									email : email
								};
							}
						}


					}
					// console.log(_this.users);

				}

				else {
					console.log(error);
				}

			});

		},

	}
};


var _this = UsersController;

module.exports = _this;
