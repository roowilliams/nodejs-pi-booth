var config = require('../config/config'),
needle = require('needle'),
events = require('events'),
config = require('../config/config');


SendController = {

	emitter : null,

	emit : function(event, args) {
		_this.emitter.emit(event, args);
	},

	on : function(event, fn) {
		_this.emitter.on(event, fn);
	},

	send : function(file, guid) {

		_this.emitter = new events.EventEmitter();

		var data = {
			photo : {
				file: file,
				content_type: 'application/octet-stream'
			},
			guid: guid
		};

		needle.post(config.sendURL, data, {
			username: config.intranetUser,
			password: config.intranetPass,
			multipart: true,
			auth: 'auto',
			follow: 3,
			'rejectUnauthorized': false
		}, function(err, resp, body) {

			if (err) {
				console.log('err: ' + err);
			}
			
			if (resp.statusCode) {

				_this.emit('response', resp.statusCode);

			}
		});

	}

};



_this = SendController;

module.exports = _this;