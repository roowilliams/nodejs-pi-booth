var gulp = require('gulp'),
    config = require('./config/config').dev,
    jshint = require('gulp-jshint'),
    scp = require('gulp-scp2'),
    compass = require('gulp-compass');
    watch = require('gulp-watch');
 

// JS hint task
gulp.task('jshint', function() {
  gulp.src(['*.js', './**/*.js', '!./node_modules/**', '!./public/js/vendor/**'])
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

var dirs = ['*.*', './**/*.*', '!./node_modules/**', '!./public/js/vendor/**'];

gulp.task('scp', function() {
	gulp.src(dirs)
	.pipe(watch(dirs))
	.pipe(scp({
		host: 'photo.tmw.co.uk',
		username: config.piUser,
		password: config.piPassword,
		dest: '/home/pi/node-pi-booth',
		watch: function(client) {
      		client.on('write', function(o) {
        	console.log('write %s', o.destination);
     		});
    	}
	}))
	.on('error', function(err) {
		console.log(err);
	});
});

gulp.task('compass', function() {
  gulp.src('./scss/*.scss')
    .pipe(compass({

    }));
});